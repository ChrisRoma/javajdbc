
package test;

import datos.*;
import domain.*;
import java.util.List;


public class TestManejoPersonas {
    
    
    public static void main(String[] args){
    PersonaDAO personaDao=new PersonaDAO();
    //nuevo objeto tipo persona
    Persona personaNueva=new Persona("ayelen","abreo","ayelen@gmail.com","554446");
    
    personaDao.insertar(personaNueva);
    Persona modificar=new Persona(1,"juan carlos","paredes","pared@gmail.com","7894561");
    personaDao.actualizar(modificar);
    Persona eliminar=new Persona(1);
    personaDao.eliminar(eliminar);
    List<Persona> personas=personaDao.seleccionar();
    
    personas.forEach(persona -> {
        System.out.println("persona: " + persona);
        });
    }
}
