package test;

import java.sql.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TestMysqlJdbc {
    public static void main(String[] args ) throws SQLException{
        String url="jdbc:mysql://localhost:3306/test?useSSl=false&useTimeZone=UTC&allowPublicKeyRetrieval=true";
        try {
            //Class.forName("com.mysql.cj.jdbc.Driver");//en algunos programas puede dar error no tener esta linea
            Connection conexion=DriverManager.getConnection(url,"root","admin");//url la cree arriba , root es el nombre del usuario por default y admin es la contraseña 
            Statement instruccion = conexion.createStatement();//esta linea sirve para cualquier base de datos
            var sql="SELECT id_persona, nombre, apellido, email, telefono FROM persona";//acostumbrarse a usar mayuscula y minuscula aunque no deberia dar error si no la usamos
            ResultSet resultado=instruccion.executeQuery(sql);
            while(resultado.next()){
                System.out.println("Id_persona: "+ resultado.getInt("id_persona"));
                System.out.println("Nombre: "+ resultado.getString("nombre"));
                System.out.println("apellido: "+ resultado.getString("apellido"));
                System.out.println("email: "+ resultado.getString("email"));
                System.out.println("telefono: "+ resultado.getString("telefono"));
                
            }
                
                
                
        }catch(SQLException ex){
            ex.printStackTrace(System.out);
        
        }
    }
}
